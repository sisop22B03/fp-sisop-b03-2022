#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <limits.h>
#include <dirent.h>
#define PORT 8080

//path
char log_path[1000] = "/home/naily/FileLog.log";
char account_path[1000] = "/home/naily/database/account_list.txt";

//fungsi prototype
void *fungsi(void *arg);
bool isFileExist(char *file);
void logging(char *query);
void createDaemons();


int main()
{	
	//loop utama
	while(1)
	{
		sleep(5);
		
		//DUNIA PER SOCKET AN :: server
		struct sockaddr_in address, new_addr;
		int fd, new_fd, ret_val;
		int opt = 1;
		socklen_t addrlen;
		pthread_t tid;
		char buf[1000];
		
		fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (fd == -1)
		{
			fprintf(stderr, "socket failed [%s]\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		
		if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
		{
			perror("setsockopt");
			exit(EXIT_FAILURE);
		}
		
		address.sin_family = AF_INET;
		address.sin_port = htons(PORT);
		address.sin_addr.s_addr = INADDR_ANY;
		
		ret_val = bind(fd, (struct sockaddr *)&address, sizeof(struct sockaddr_in));
		if (ret_val != 0)
		{
			fprintf(stderr, "bind failed");
			close(fd);
			exit(EXIT_FAILURE);
		}
		
		ret_val = listen(fd, 5);
		if (ret_val != 0)
		{
			fprintf(stderr, "listen failed");
			close(fd);
			exit(EXIT_FAILURE);
		}
		
		new_fd = accept(fd, (struct sockaddr *)&new_addr, &addrlen);
		
		if (new_fd >= 0)
		{
			pthread_create(&tid, NULL, &fungsi, (void *)&new_fd);
		}
		
		
	}
	return 0;
}

//membuat daemons
void createDaemons()
{
	pid_t pid, sid;
	
	pid = fork(); // Menyimpan PID dari Child Process
	
	//Keluar saat fork gagal
  	//(nilai variabel pid < 0) 
  	if (pid < 0) 
	{
    	exit(EXIT_FAILURE);
  	}
  	
  	//Mengubah Mode File dengan umask
  	umask(0);
  	
  	
  	//Membuat Unique Session ID (SID)
  	sid = setsid();
  	if (sid < 0) 
	{
  		exit(EXIT_FAILURE);
	}	
	
	//mengubah working directory
	if ((chdir("/")) < 0) 
	{
  		exit(EXIT_FAILURE);
	}
	
	//menutup file desciptor standar
	close(STDERR_FILENO);
	close(STDIN_FILENO);
	close(STDOUT_FILENO);
}


//semua ddl, dml, dll
void *fungsi(void *arg)
{
	int fd = *(int *)arg;
	char query[1000], command[1000], path[1000];
	
	while(read(fd, query, 1000) != 0)
	{
		
		puts(query);
		strcpy(command, query);
		
		//nanti LOGGING dulu DISINI
		logging(query);
		
		char *tkn = strtok(command, " ");
		
		
		//BUAT LOGIN
		
		
		//Data Definition Language (DDL)
		//BUAT CREATE DATABASE DAN TABLE
		//DATABASE
		if (strcmp(tkn, "CREATE") == 0)
		{
			tkn = strtok(NULL, " ");
			
			if(strcmp(tkn, "DATABASE") == 0)
			{
				char cwd[PATH_MAX], temp_dir[PATH_MAX], db_dir[6000], root[6000];
				char *token = strtok(NULL, " ");
				
				//CURRENT WORKING DIRECTORY
				getcwd(cwd, sizeof(cwd));
				//printf("tes2 %s", cwd);
				strcpy(temp_dir, cwd);
				
				//kembbali ke parent apabila sedang USE Database
				if (strstr(cwd, "databases/") != NULL)
				{
					chdir("../../");
				}
				
				if (getcwd(cwd, sizeof(cwd)) != NULL)
				{
					char *dbName = strtok(token, ";");
					
					//buat directory
					sprintf(db_dir, "%s/databases/%s", cwd, dbName);
					sprintf(root, "%s/databases", cwd);
					
					//membuat directory database
					DIR *dir = opendir(root);
					if(ENOENT == errno)
					{
						mkdir(root, 0777);
					}
					
					//membuat database
					int cek = mkdir(db_dir, 0777);
					if(cek == 0)
					{
						printf("CREATE DB SUCCESS!!!\n");
					}
					else
					{
						printf("CREATE DB FAILED!!!\n");
					}
				}
				
			}
			else if(strcmp(tkn, "TABLE") == 0)
			{
				char table_dir[6000], cwd[PATH_MAX];
				char *table_name = strtok(NULL, " ");
				
				printf("Table Name: %s\n", table_name);
				if(getcwd(cwd, sizeof(cwd)) != NULL)
				{
					FILE *tabel_file;
					char *attribut = strtok(NULL, "()");
					
					printf("List of Column :%s\n", attribut);
					sprintf(table_dir, "%s/%s", cwd, table_name);
					
					printf("dir %s\n", table_dir);
					
					if (strstr(cwd, "databases/") == NULL)
					{
						printf("BELUM ADA DATABASE YANG DIGUNAKAN!!!\n");
					}
					else if(isFileExist(table_dir))
					{
						printf("TABEL SUDAH ADA SEBELUMNYA!!!\n");
					}
					else if(attribut == NULL)
					{
						printf("INVALID SYNTAX!!!\n");
					}
					else //membuat tabel
					{
						tabel_file = fopen(table_dir, "a");
						
						fprintf(tabel_file, "%s\n", attribut);
						fclose(tabel_file);
						printf("CREATE TABLE SUCCESS!!!\n");
					}
				}
			}
			//CREATE USER username IDENTIFIED BY password;
			else if(strcmp(tkn, "USER") == 0)
			{
				char *username  = strtok(NULL, " ");
				char *token2  = strtok(NULL, " ");
				char *token3  = strtok(NULL, " ");
				char *password = strtok(NULL, ";");
			
				FILE *file = fopen(account_path, "a+");
				if(file)
				{
					fprintf(file, "%s, %s\n", username, password);
					printf("CREATE USER SUCCESS!!!\n");
				}
				
				fclose(file);		
			}
			else
			{
				printf("INVALID CREATE QUERY!!!\n");
			}
		}
		
		// DROP DATABASE DAN TABEL
		else if(strcmp(tkn, "DROP") == 0)
		{
			char *token = strtok(NULL, " ");
			
			if(strcmp(token, "DATABASE") == 0)
			{
				char db_name[5000], cwd[PATH_MAX], dir[PATH_MAX], *cek;
				token = strtok(NULL, " ");
				
				getcwd(cwd, sizeof(cwd));
				strcpy(dir, cwd);
				
				//kembali ke parent kalau lg use
				cek = strstr(cwd, "databases/");
				if (cek != NULL)
				{
					chdir("../../");
				}
				
				if(getcwd(cwd, sizeof(cwd)) != NULL)
				{
					DIR *dp;
					struct dirent *ep;
					char path[100], file_dir[6000];
					char *token2 = strtok(token, ";");
					
					//drop database recursively
					//hapus isinya
					sprintf(db_name, "%s/databases/%s", cwd, token2);
					dp = opendir(db_name);
					if(dp != NULL)
					{
						while ((ep = readdir(dp)))
						{
							if (ep->d_type == DT_REG)
							{
								puts(ep->d_name);
								sprintf(file_dir, "%s/%s", db_name, ep->d_name);
								remove(file_dir);
							}
						}
						(void) closedir(dp);
					}
					//hapus databasenya
					int cek = rmdir(db_name);
					if(!cek)
					{
						printf("DROP DATABASE SUCCESS!!!\n");
						chdir(dir);
					}
					else
					{
						printf("DROP DATABASE FAILED!!!\n");
					}
				}
			}
			//DROP TABLE
			else if(strcmp(tkn, "TABLE") == 0)
			{
				char table[5000], cwd[PATH_MAX];
				token = strtok(NULL, " ");
				
				if (getcwd(cwd, sizeof(cwd)) != NULL)
				{
					char *token2 = strtok(token, ";");
					sprintf(table, "%s/%s", cwd, token2);
					
					if (strstr(cwd, "databases/") == NULL)
					{
						printf("BELUM MENGGUNAKAN DATABASE!!!\n");
					}
					else
					{
						int cek = remove(table);
						if(!cek)
						{
							printf("DROP TABLE SUCCESS\n");
						}
						else
						{
							printf("DROP TABLE FAILED");
						}
					}
				}
				
			}
			else
			{
				printf("INVALID DROP QUERY!!!\n");
			}	
		}
		
		//Authorisasi
		//USE :: Untuk dapat mengakses database yang dia punya permission dengan command. 
		else if(strcmp(tkn, "USE") == 0)
		{
			char cwd[PATH_MAX], db_dir[6000], *check;
			char *token = strtok(NULL, " ");
			
			getcwd(cwd, sizeof(cwd));
			
			check = strstr(cwd, "databases");
			//kembali ke parent
			if(check != NULL)
			{
				chdir("../../");
			}
			
			if (getcwd(cwd, sizeof(cwd)) != NULL)
			{
				char *db = strtok(token, ";");
				
				printf("%s\n", db);
				sprintf(db_dir, "%s/databases/%s", cwd, token);
				printf("dir: %s\n", db_dir);
				
				//change the current working directory
				int cek = chdir(db_dir);
				if(!cek)
				{
					printf("CHANGE DATABASE SUCCESS!!!\n");
				}
				else
				{
					printf("CHANGE DATABASE FAILED!!!\n");
				}
			}
		}
		
		//Data Manipulation Language (DML)
		//INSERT
		else if(strcmp(tkn, "INSERT") == 0)
		{
			tkn = strtok(NULL, " ");
			if(strcmp(tkn, "INTO") == 0)
			{
				char table_dir[5000], cwd[PATH_MAX];
				char *table_name = strtok(NULL, " ");
				
				if (getcwd(cwd, sizeof(cwd)) != NULL)
				{
					FILE *file;
					char *values = strtok(NULL, "()");
					sprintf(table_dir, "%s/%s", cwd, table_name);
					
					if (strstr(cwd, "databases/") == NULL)
					{
						printf("BELUM MENGGUNAKAN DATABASE!!!\n");
					}
					else if(values == NULL)
					{
						printf("INVALID SYNTAX!!!\n");
					}
					else if(isFileExist(table_dir))
					{
						file = fopen(table_dir, "a");
						
						fprintf(file, "%s\n", values);
						fclose(file);
						printf("INSERT INTO %s SUCCESS!!!\n", table_name);
					}
					else
					{
						printf("INSERT INTO %s FAILED\n", table_name);	
					}
				}
			}
			else
			{
				printf("INVALID QUERY!!!\n");
			}
		}
		//UPDATE
		//-------
		//DELETE
		else if(strcmp(tkn, "DELETE") == 0)
		{
			tkn = strtok(NULL, " ");
			
			if(strcmp(tkn, "FROM") ==0 )
			{
				char table[5000], cwd[PATH_MAX];
				char *token = strtok(NULL, " ");
				
				
				 if (getcwd(cwd, sizeof(cwd)) != NULL)
				 {
				 	FILE *file;
				 	char header[1000];
				 	
				 	char *token2 = strtok(token, ";");
				 	sprintf(table, "%s/%s", cwd, token2);
				 	
				 	if (strstr(cwd, "databases/") == NULL)
					{
						printf("BELUM MENGGUNAKAN DATABASE!!!\n");
					}
					else
					{
						file = fopen(table, "r");
						
						if(file)
						{
							//ambil header atau nama kolom2nya itu
							fgets(header, 100, file);
							fclose(file);
							
							//timpa
							file = fopen(table, "w");
							fprintf(file, "%s\n", header);
							printf("DELETE DATA SUCCESS!!!\n");
							fclose(file);
						}
						else
						{
							printf("DELETE DATA FAILED!!!\n");
						}
					}
				 }
			}
		}
		//SELECT
		else if(strcmp(tkn, "SELECT") == 0)
		{
			char table[5000], cwd[PATH_MAX];
			tkn = strtok(NULL, " ");
			
			if(strcmp(tkn, "*") ==0 )
			{
				char *token = strtok(NULL, " ");
				
				token = strtok(NULL, ";");
				
				if (getcwd(cwd, sizeof(cwd)) != NULL)
				{
				 	FILE *file;
				 	printf("%s\n", table);
				 	sprintf(table, "%s/%s", cwd, token);
				 	
				 	if (strstr(cwd, "databases/") == NULL)
					{
						printf("BELUM MENGGUNAKAN DATABASE!!!\n");
					}
					else
					{
						file = fopen(table, "r");
						
						if(file == NULL)
						{
							printf("TABLE NOT EXIST!!!\n");
						}
						else if(file)
						{
							const unsigned MAX_LENGTH = 1000;
							char buffer[MAX_LENGTH];
							
							while(fgets(buffer, MAX_LENGTH, file))
							{
								printf("%s", buffer);
							}
							
							printf("SELECT DATA SUCCESS!!!\n");
							fclose(file);
						}
						else
						{
							printf("SELECT DATA FAILED!!!\n");
						}
					}
				}
					
			}
			
		}
		//WHERE
		//-------
	}
}


//pengecekan apabila sudah ada suatu file belum
bool isFileExist(char *file)
{
	struct stat buffer;
	return (stat(file, &buffer) == 0);
}


//LOGGING
void logging(char *query)
{
	FILE *file = fopen(log_path, "a+");
	
	time_t rawtime;
	struct tm *info;
	char buffer[1000];
		
	time(&rawtime);
		
	info = localtime(&rawtime);
		
	strftime(buffer, sizeof(buffer), "%Y-%m-%d %X", info);
	fprintf(file, "%s:root:%s\n", buffer, query);
	fclose(file);
	
}
