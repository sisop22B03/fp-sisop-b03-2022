//B03

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>

#define PORT 8080

void *inCtrl(void *client_fd);
void *outCtrl(void *client_fd);

int main(int argc, char *argv[])
{
	//socket::client
	pthread_t tid[2];
	struct sockaddr_in address;
	int client_fd;
	int opt = 1;
	struct hostent *host;

	if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
	{
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
	{
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	address.sin_family = AF_INET;
	address.sin_port = htons(PORT);
	host = gethostbyname("127.0.0.1");
	address.sin_addr = *((struct in_addr *)host->h_addr);

	if (connect(client_fd, (struct sockaddr *)&address, sizeof(struct sockaddr_in)) == -1)
		exit(EXIT_FAILURE);

	//thread untuk menggabungkan fungsi inCtrl dan outCtrl
	pthread_create(&(tid[0]), NULL, &inCtrl, (void *)&client_fd);
	pthread_create(&(tid[1]), NULL, &outCtrl, (void *)&client_fd);

	pthread_join(tid[0], NULL);
	pthread_join(tid[1], NULL);

	close(client_fd);
	return 0;
}


//mengirim ke server
void *inCtrl(void *client_fd)
{
	char s_query[1000] = {0};
	int fd = *(int *)client_fd;

	printf("DATABASE B03\n");

	while (1)
	{
		fgets(s_query, 1000, stdin);
		char *token = strtok(s_query, "\n");

		if (token != NULL)
		{
			strcpy(s_query, token);
		}

		if (strcmp(s_query, "exit") == 0)
		{
			exit(EXIT_SUCCESS);
		}
		
		send(fd, s_query, sizeof(char) * 1000, 0);
	}
}

//digunakan untuk menerima dari server
void *outCtrl(void *client_fd)
{
	char r_query[1000] = {0};
	int fd = *(int *)client_fd;

	while (1)
	{
		memset(r_query, 0, sizeof(char) * 1000);

		if (recv(fd, r_query, 1000, 0) == 0)
			exit(EXIT_SUCCESS);

		printf("%s", r_query);
		fflush(stdout);
	}
}
